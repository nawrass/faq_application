package com.gfi.faq.service;

import com.gfi.faq.model.Question;

import java.util.List;

public interface QuestionService {
    List<Question> FindAllQuestion();

    Question createQuestion(Question question) throws Exception;

    boolean deleteQuestion(Long id);

    Question updateQuestion(Question question ,Long id) throws Exception;

    List<Question> searchQuestion(String question)throws Exception;

}
