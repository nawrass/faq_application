package com.gfi.faq.service.Impl;

import com.gfi.faq.dao.QuestionRepository;
import com.gfi.faq.dao.UserRepository;
import com.gfi.faq.model.Question;
import com.gfi.faq.model.User;
import com.gfi.faq.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
     QuestionRepository questionRepository;
     @Autowired
     UserRepository userRepository;

    @Override
    public List<Question> FindAllQuestion() {
        return this.questionRepository.findAll();
    }

    @Override
    public Question createQuestion(Question question) throws Exception {



        return this.questionRepository.save(question);

    }

    @Override
    public boolean deleteQuestion(Long id) {
        Optional<Question> question = this.questionRepository.findById(id);
        if (question.isPresent()) {
            this.questionRepository.deleteById(id);
            return true;
        } else
            return false;
    }

    @Override
    public Question updateQuestion(Question question,Long id) throws Exception {
        Optional<Question> question1=this.questionRepository.findById(id);

            question.setId(id);
          return  questionRepository.save(question);


    }

    @Override
    public List<Question> searchQuestion(String question)throws Exception{
        return this.questionRepository.findByQuestion(question);
    }
}
