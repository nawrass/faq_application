package com.gfi.faq.dto;

public class QuestionDTO {
    private Long id;
    private String question;
    private String reponses;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getReponses() {
        return reponses;
    }

    public void setReponses(String reponses) {
        this.reponses = reponses;
    }

    public QuestionDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QuestionDTO(Long id, String question, String reponses) {
        this.id = id;
        this.question = question;
        this.reponses = reponses;
    }
}
