package com.gfi.faq.mapper;

import com.gfi.faq.dto.QuestionDTO;
import com.gfi.faq.model.Question;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface QuestionMapper {
    QuestionDTO questionDTO(Question question);

    Question QuestionDTOToQuestion(QuestionDTO dto);
}
