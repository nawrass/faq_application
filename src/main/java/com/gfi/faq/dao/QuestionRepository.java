package com.gfi.faq.dao;

import com.gfi.faq.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
//    List<Question> findByQuestion(String question);
    @Query("from Question h where lower(h.question) like CONCAT('%', lower(:contains), '%')")
    public List<Question> findByQuestion(@Param("contains") String question);
}
