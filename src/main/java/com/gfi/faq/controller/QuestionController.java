package com.gfi.faq.controller;

import com.gfi.faq.dao.UserRepository;
import com.gfi.faq.dto.QuestionDTO;

import com.gfi.faq.model.Question;
import com.gfi.faq.model.User;
import com.gfi.faq.service.QuestionService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/question")
public class QuestionController {
    @Autowired
    QuestionService questionService;
    @Autowired
    UserRepository userRepository;

    
    @GetMapping
    @HystrixCommand(fallbackMethod = "getDataFallBack", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "500")
    },
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "30"),
                    @HystrixProperty(name = "maxQueueSize", value = "101"),
                    @HystrixProperty(name = "keepAliveTimeMinutes", value = "2"),
                    @HystrixProperty(name = "queueSizeRejectionThreshold", value = "15"),
                    @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "12"),
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "1440")
            })

    public List<Question> getAllQuestion() {
        System.out.println("before exception");
        List<Question> questions=this.questionService.FindAllQuestion();

        System.out.println("after exception");
        return questions;
    }

    @GetMapping("/withfein")
   /* @HystrixCommand(fallbackMethod = "getDataFallBack", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "500")
    },
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "30"),
                    @HystrixProperty(name = "maxQueueSize", value = "101"),
                    @HystrixProperty(name = "keepAliveTimeMinutes", value = "2"),
                    @HystrixProperty(name = "queueSizeRejectionThreshold", value = "15"),
                    @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "12"),
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "1440")
            })*/

    public List<Question> getAllQuestionwithfein() {
        System.out.println("before  8089");
        List<Question> questions=this.questionService.FindAllQuestion();
        return questions;
    }

    public List<Question> getDataFallBack() {

        System.out.println("Inside fallback");
        List<Question> questions=new ArrayList<>();
        Question question=new Question();
        question.setQuestion("quant?");
        question.setReponses("nn c'est quant");
        questions.add(question);
        return questions;


    }

    @PostMapping("/post")
    public ResponseEntity<Question> createQuestion(@RequestBody Question question) {

//        Question question=questionMapper.QuestionDTOToQuestion(questionDTO);
//        questionService.createQuestion(question);
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(questionDTO);
        try {

            this.questionService.createQuestion(question);
            return new ResponseEntity<Question>(question, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<Question>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable("id") Long id) {
        questionService.deleteQuestion(id);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
    @PutMapping(path = "/{id}")
    public  ResponseEntity<Question> updateQuestion(@RequestBody Question question ,@PathVariable("id") Long id) {
        try {
            questionService.updateQuestion(question,id);
            return new ResponseEntity<Question>(question, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<Question>(HttpStatus.NOT_ACCEPTABLE);
        }
    }
    @GetMapping("/tutorials")
    public List<Question> searchQuestion( @RequestParam("contains") String question) {


        try {
            return this.questionService.searchQuestion(question);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}
