package com.gfi.faq.model;

import javax.persistence.*;

@Entity
public class Question {
    @Id
    @GeneratedValue
    private Long id;
    private String question;
    @Column(columnDefinition="TEXT")
    private String reponses;
    @ManyToOne
     private User user;


    public String getReponses() {
        return reponses;
    }

    public void setReponses(String reponses) {
        this.reponses = reponses;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getReponse() {
        return reponses;
    }

    public void setReponse(String reponse) {
        this.reponses = reponse;
    }

    public Question(String question, String reponses) {
        this.question = question;
        this.reponses = reponses;
    }

    public Question() {
    }
}
